import re
import sys
import datetime
import argparse

from twitch import Helix


DEBUG = False

# patterns search
KEY_WORDS = ["AH", "LMAO", "LOL"]

PATTERNS = []
for w in KEY_WORDS:
    PATTERNS.append(re.compile(r'^{pattern}|\s{pattern}'.format(pattern=w),re.IGNORECASE))

PATTERN_MIN_MESSAGES = 3
PATTERN_MAX_INTERVAL_BETWEEN_MSG = 5.0
PATTERN_MAX_EMPTY_MSG = 15


class CommentsAnalyzer(object):
    def __init__(self, video_id, client_id):
        """
        :param video_id:  Twitch Video ID
        :param client_id: Twitch Client ID
        """

        self.helix = Helix(client_id=client_id, use_cache=True)
        self.videos = self.helix.videos(video_id)

    def analyze(self):
        """
        :return: time intervals [[<start time interval>, <end time interval>], ...]
        :rtype: list
        """
        content_start_offset_seconds = 0.0
        content_offset_seconds = 0.0
        content_start_count = 0
        content_empty_msg = 0
        content_users = []
        messages = []

        for video in self.videos:
            for comment in video.comments:
                exist_pattern = False
                need_restart = False

                for p in PATTERNS:
                    if len(p.findall(comment.message.body)):
                        exist_pattern = True
                        break

                if not exist_pattern:
                    if content_start_offset_seconds and DEBUG:
                        messages.append("- {} {}".format(str(datetime.timedelta(seconds=comment.content_offset_seconds)),
                                       comment.message.body))
                    content_empty_msg += 1

                # check empty messages
                if not need_restart and content_empty_msg >= PATTERN_MAX_EMPTY_MSG:
                    need_restart = True

                if exist_pattern and content_offset_seconds:
                    # check start interval pattern
                    if content_start_count < PATTERN_MIN_MESSAGES:
                        # check interval between messages
                        if not need_restart and comment.content_offset_seconds - content_offset_seconds > PATTERN_MAX_INTERVAL_BETWEEN_MSG:
                            need_restart = True
                        # check unique users in interval
                        if not need_restart and content_start_count < PATTERN_MIN_MESSAGES:
                            if comment.user in content_users:
                                need_restart = True

                # restart to search interval
                if need_restart:
                    if content_start_count >= PATTERN_MIN_MESSAGES:
                        yield content_start_offset_seconds, content_offset_seconds
                    content_start_offset_seconds = 0
                    content_start_count = 0
                    content_users = []

                if exist_pattern:
                    content_empty_msg = 0
                    content_offset_seconds = comment.content_offset_seconds
                    if not content_start_offset_seconds:
                        messages = []
                        content_start_offset_seconds = content_offset_seconds
                    if DEBUG:
                        messages.append("+ {} {}".format(
                            str(datetime.timedelta(seconds=content_offset_seconds)),
                            comment.message.body)
                        )
                    content_users.append(comment.user)
                    content_start_count += 1

        if content_start_count >= PATTERN_MIN_MESSAGES:
            yield content_start_offset_seconds, content_offset_seconds


def main():
    parser = argparse.ArgumentParser(description="Searching for patterns in intervals")
    parser.add_argument("--video", type=int, help="Twitch Video ID", required=True)
    parser.add_argument("--client", type=str, help="Twitch Client ID", default="hdaoisxhhrc9h3lz3k24iao13crkkq8", required=False)
    args = parser.parse_args()

    ca = CommentsAnalyzer(args.video, args.client)
    for stime, etime in ca.analyze():
        sys.stdout.write("{},{}\n".format(stime, etime))


if __name__ == "__main__":
    main()